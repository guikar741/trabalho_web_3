"""Modelos do Sistema."""
from flask_marshmallow import Marshmallow
from flask_marshmallow.fields import fields
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from passlib.hash import pbkdf2_sha256
db = SQLAlchemy()
ma = Marshmallow()


def config_db_ma(app):
    """Configuração das bibliotecas no app."""
    db.init_app(app)
    ma.init_app(app)
    app.db = db
    Migrate(app, app.db)


class Disciplinas(db.Model):
    """Model das Disciplinas."""

    __tablename__ = "disciplinas"

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    curso = db.Column(db.String(50), nullable=False)
    topicos = db.relationship('Topicos', backref='disciplina', lazy=True)

    def __init__(self, nome=None, curso=None, *args, **kwargs):
        """Inicializador do Model."""
        super().__init__(*args, **kwargs)
        self.nome = nome
        self.curso = curso

    def __repr__(self):
        """Representação do Model."""
        return '<Disciplina %r>' % self.nome


class Topicos(db.Model):
    """Model dos Topicos."""

    __tablename__ = "topicos"

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    disciplina_id = db.Column(
        db.Integer,
        db.ForeignKey(
            'disciplinas.id',
            onupdate="CASCADE",
            ondelete="SET NULL"
        ),
        nullable=False
    )

    def __init__(self, nome=None, *args, **kwargs):
        """Inicializador do Model."""
        super().__init__(*args, **kwargs)
        self.nome = nome

    def __repr__(self):
        """Representação do Model."""
        return '<Topico %r>' % self.nome


class Relacionamentos(db.Model):
    """Model dos Relacionamentos."""

    __tablename__ = 'relacionamentos'
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    topico1_id = db.Column(
        db.Integer,
        db.ForeignKey(
            'topicos.id',
            onupdate="CASCADE",
            ondelete="SET NULL"
        )
    )
    topico2_id = db.Column(
        db.Integer,
        db.ForeignKey(
            'topicos.id',
            onupdate="CASCADE",
            ondelete="SET NULL"
        )
    )
    topico1 = db.relationship(
        "Topicos",
        primaryjoin=Topicos.id == topico1_id,
    )
    topico2 = db.relationship(
        "Topicos",
        primaryjoin=Topicos.id == topico2_id,
    )

    def __init__(self, nome=None, *args, **kwargs):
        """Inicializador do Model."""
        super().__init__(*args, **kwargs)
        self.nome = nome

    def __repr__(self):
        """Representação do Model."""
        return '<Relacionamento %r>' % self.nome


class Usuarios(db.Model):
    """Model dos Usuarios."""

    __tablename__ = "usuarios"

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(150), unique=True, nullable=False)
    senha = db.Column(db.String(200), nullable=False)
    level = db.Column(db.Integer, nullable=False, default="1")

    def __init__(self, email=None, senha=None, level=1, *args, **kwargs):
        """Inicializador do Model."""
        super().__init__(*args, **kwargs)
        self.email = email
        self.senha = senha
        self.level = level

    def hash_senha(self):
        """Criptografa Senha."""
        self.senha = pbkdf2_sha256.hash(self.senha)

    def verificar_senha(self, senha):
        """Verifica se a Senha está Correta."""
        return pbkdf2_sha256.verify(senha, self.senha)

    def __repr__(self):
        """Representação do Model."""
        return '<User %r>' % self.email


class UsuariosSchema(ma.Schema):
    """Schema para serialização do Usuarios."""

    class Meta:
        """Metadados do Schema."""

        model = Usuarios

    email = fields.Str(required=True)
    senha = fields.Str(required=True)
    level = fields.Integer(load_only=True)


class DisciplinasSchema(ma.Schema):
    """Schema para serialização das disciplinas."""

    class Meta:
        """Metadados do Schema."""

        model = Disciplinas

    id = fields.Integer(dump_only=True)
    nome = fields.Str(required=True)
    curso = fields.Str(required=True)


class TopicosSchema(ma.Schema):
    """Schema para serialização dos topicos."""

    class Meta:
        """Metadados do Schema."""

        model = Topicos

    id = fields.Integer(dump_only=True)
    nome = fields.Str(required=True)
    disciplina_id = fields.Integer(required=True, load_only=True)
    disciplina = fields.Nested(DisciplinasSchema, allow_none=True)


class RelacionamentosSchema(ma.Schema):
    """Schema para serialização dos relacionamentos."""

    class Meta:
        """Metadados do Schema."""

        model = Relacionamentos

    id = fields.Integer(dump_only=True)
    nome = fields.Str(required=True)
    topico1_id = fields.Integer(required=True, load_only=True)
    topico2_id = fields.Integer(required=True, load_only=True)
    topico1 = fields.Nested(TopicosSchema, allow_none=True, dump_only=True)
    topico2 = fields.Nested(TopicosSchema, allow_none=True, dump_only=True)
