"""Arquivo de Configurações."""
from decouple import config
import os


class Config:
    """Classe Base das Configurações."""

    PROJECT_ROOT = os.path.dirname(__file__)
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    JWT_SECRET_KEY = config("JWT_SECRET_KEY")
    SQLALCHEMY_DATABASE_URI = f"{config('SQLALCHEMY_DATABASE_TYPE')}/" + \
                              f"{PROJECT_ROOT}/{config('SQLALCHEMY_DATABASE_NAME')}"
    SQLALCHEMY_ECHO = True
