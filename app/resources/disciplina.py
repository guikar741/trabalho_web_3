"""Recursos das Diciplinas."""
from flask import current_app
from flask_restful import Resource, abort
from flask_restful.reqparse import RequestParser
from flask_jwt_extended import jwt_required, get_jwt_identity
from ..models import Disciplinas, DisciplinasSchema, Usuarios
from marshmallow.exceptions import ValidationError


def verificaNivel(func):
    """Verificação do nivel de usuario."""
    def verifica(*args, **kwargs):
        user = Usuarios.query.get(get_jwt_identity())
        if user and user.level != 1:
            abort(403, status="Você não possui permissão para acessar!")
        return func(*args, **kwargs)
    return verifica


class DisciplinaBase(Resource):
    """Classe Base das Disciplinas."""

    parser = RequestParser()
    parser.add_argument("nome")
    parser.add_argument("curso")


class DisciplinaTodas(DisciplinaBase):
    """Classe para Mostrar Todas Disciplinas."""

    @jwt_required
    def get(self):
        """Retorna todas as Disciplinas."""
        return {'disciplinas': DisciplinasSchema(many=True).dump(Disciplinas.query.all())}


class Disciplina(DisciplinaBase):
    """Classe Para Executar CRUD das Disciplinas."""

    @jwt_required
    def get(self, id=None):
        """Read de uma disciplina."""
        if id == None:
            return {"status": "disciplina invalida!"}, 400
        disciplina = Disciplinas.query.get(id)
        return ({'disciplina': DisciplinasSchema().dump(disciplina)}, 200) if disciplina else \
            ({"status": "disciplina não encontrada!"}, 400)

    @jwt_required
    @verificaNivel
    def post(self, id=None):
        """Create de uma disciplina."""
        try:
            if id:
                return {"status": "erro não envie o id de uma disciplina!"}, 400
            dados = DisciplinasSchema().load(self.parser.parse_args())
            disciplina = Disciplinas(**dados)
            current_app.db.session.add(disciplina)
            current_app.db.session.commit()
            return {
                "status": "cadastrado!",
                "disciplina": DisciplinasSchema().dump(disciplina)
            }, 201
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    @verificaNivel
    def put(self, id=None):
        """Update de uma disciplina."""
        try:
            if id == None:
                return {"status": "disciplina invalida!"}, 400
            dados = DisciplinasSchema().load(self.parser.parse_args())
            disciplina = Disciplinas.query.get(id)
            if disciplina:
                disciplina.nome = dados['nome']
                disciplina.curso = dados['curso']
                current_app.db.session.add(disciplina)
                current_app.db.session.commit()
                return {
                    "status": "atualizado!",
                    "disciplina": DisciplinasSchema().dump(disciplina)
                }
            return {"status": "disciplina não encontrada!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    @verificaNivel
    def delete(self, id=None):
        """Delete de uma disciplina."""
        if id == None:
            return {"status": "disciplina invalida!"}, 400
        disciplina = Disciplinas.query.get(id)
        if disciplina:
            current_app.db.session.delete(disciplina)
            current_app.db.session.commit()
            return {
                "status": "deletado!"
            }
        return {"status": "erro disciplina não encontrada!"}
